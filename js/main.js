angular.module('ui.bootstrap.demo', ['ui.bootstrap']);
angular.module('ui.bootstrap.demo').controller('ModalDemoCtrl', function ($scope, $modal, $log, $http) {

    $scope.user = {name: ""} //input models the value here
    $scope.items = [];
    $scope.getEvents = function(){
        $http({
            url: "http://qa-demo.ticketsocket.com/api/v1/events",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            method: 'GET',
        }).then(function(response){

            var length = response.data.data.length;
            for(var i = 0; i<length; i++) {//for each item in the response array
                var title = response.data.data[i].h1AndTitle;
                var curId = response.data.data[i].id;
                //$log.info(title, id);
                var curObj = {eventTitle: title, id: curId};
                $scope.items.push(curObj);
            }

            $log.info($scope.items);
            $scope.open('lg');



        }, function(response){
            //$log.warn(response);
        });
    };

    $scope.open = function () {

        var modalInstance = $modal.open({ //this is the scope within the controller below
            templateUrl: 'myModalContent.html',
            controller: 'ModalInstanceCtrl', //we assign a controller , resolve here acts as glue between controller and parent
            resolve: {
                user: function () {  //setting the parents user property
                    return $scope.user;
                }
            }
        });

        modalInstance.result.then(function () {
            $scope.user.name = user.name;
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };
});

// Please note that $modalInstance represents a modal window (instance) dependency.
// It is not the same as the $modal service used above.

angular.module('ui.bootstrap.demo').controller('ModalInstanceCtrl', function ($scope, $modalInstance, user) {
    $scope.user = user; //we pass in the user object from parent controller as a value to $scope.user

    $scope.ok = function () {
        $modalInstance.close();
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});


