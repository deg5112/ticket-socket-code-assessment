var app = angular.module('ui.bootstrap.demo', ['ngAnimate', 'ui.bootstrap']);
app.controller('ModalDemoCtrl', function ($scope, $uibModal, $log, $http) {
    $scope.items = [];
    $scope.animationsEnabled = true;


    $scope.getEvents = function(){
        $http({
            url: "http://qa-demo.ticketsocket.com/api/v1/events",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            method: 'GET',
        }).then(function(response){

            var length = response.data.data.length;
            for(var i = 0; i<length; i++) {//for each item in the response array
                var title = response.data.data[i].h1AndTitle;
                var curId = response.data.data[i].id;
                //$log.info(title, id);
                var curObj = {eventTitle: title, id: curId};
                $scope.items.push(curObj);
            }

            $log.info($scope.items);
            $scope.open('lg');



        }, function(response){
            $log.warn(response);
        });
    };


    $scope.open = function (size) {//opens modal with parameters specified
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'myModalContent.html',  //which template to use
            controller: 'ModalInstanceCtrl',  //defined below
            size: size,
            resolve: {
                items: function () {
                    return $scope.items;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };

    $scope.toggleAnimation = function () {
        $scope.animationsEnabled = !$scope.animationsEnabled;
    };
});

// Please note that $uibModalInstance represents a modal window (instance) dependency.
// It is not the same as the $uibModal service used above.

app.controller('ModalInstanceCtrl', function ($scope, $uibModalInstance, items, $log){ //controller to give scope methods to close and dismiss modal
    //this object gets instantiated when you open the modal
    $scope.items =  items;
    console.log($scope.items, 'scopeItems'); //set scopeItems = array


    $scope.ok = function () {  //
        $uibModalInstance.close($scope.selected.item);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.consoleSelected = function(selected){
        $log.info(selected);
    };

    $scope.selected = null;  //is modeled to the selected value



});